package rest_api;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class APIController {
    @Autowired
    SectionRepository repository;

    private long jobId = 0;

    /**
     * api to upload xls file and parse it in to jpa in memmory database
     *
     * @param file xls file with default format
     * @return jobId to get the result
     */
    @RequestMapping(value = "/register-job", method = RequestMethod.POST)
    public String registerJob(@RequestParam("file") MultipartFile file) {
        String fileName = file.getOriginalFilename();
        if (!file.isEmpty()) {
            try {
                Boolean fileTypeCorrect = fileName.endsWith("xls") || fileName.endsWith("XLS");
                if (!fileTypeCorrect) {
                    return "You uploaded no xls file!";
                }
                return Long.toString(parseFile(file));
            } catch (Exception e) {
                return "You failed to upload " + fileName + " => " + e.getMessage();
            }
        } else {
            return "You failed to upload " + fileName + " because the file was empty.";
        }
    }

    /**
     * api to get all sections from register-job
     *
     * @param jobId from api register-job
     * @return sections
     */
    @RequestMapping(value = "/result/id", method = RequestMethod.GET)
    public List getResultById(@RequestParam(value = "jobId") Long jobId) {
        List sections = repository.findByJobId(jobId);
        return sections;
    }

    /**
     * api to get all section with given name
     *
     * @param name name of section
     * @return sections
     */
    @RequestMapping(value = "/result/name", method = RequestMethod.GET)
    public List getResultByName(@RequestParam(value = "name") String name) {
        List sections = repository.findBySectionName(name);
        return sections;
    }

    /**
     * api to get all sections with given ClassCode of Geodata
     *
     * @param code ClassCode of Geodata
     * @return sections
     */
    @RequestMapping(value = "/result/code", method = RequestMethod.GET)
    public ArrayList<Section> getResult(@RequestParam(value = "code") String code) {
        ArrayList sections = new ArrayList();
        Iterable<Section> allSections = repository.findAll();
        for (Section section : allSections) {
            for (Geodata geodata : section.getGeodata()) {
                if (geodata.getClassCode().contentEquals(code)) {
                    sections.add(section);
                }
            }
        }
        return sections;
    }

    /**
     * Method to parse xls file in jpa in memmory database
     *
     * @param file xls file with default format
     * @return jobId from this parse Task
     * @throws IOException if file is not correct
     */
    private long parseFile(MultipartFile file) throws IOException {
        Workbook workbook = new HSSFWorkbook(file.getInputStream());
        Sheet sheet = workbook.getSheetAt(0);
        Map<Integer, List<String>> data = new HashMap<>();
        int i = 0;
        for (Row row : sheet) {
            data.put(i, new ArrayList<String>());
            for (Cell cell : row) {
                data.get(i).add(cell.getStringCellValue());
            }
            i++;
        }
        for (int j = 1; j < data.keySet().size(); j++) {
            List<String> currentRow = data.get(j);
            Section newSection = new Section();
            newSection.setSectionName(currentRow.get(0));
            for (int k = 1; k < currentRow.size(); k += 2) {
                Geodata geoNew = new Geodata(currentRow.get(k), currentRow.get(k + 1));
                newSection.addGeodata(geoNew);
            }
            newSection.setJobId(jobId);
            repository.save(newSection);
        }
        return this.jobId++;
    }

}

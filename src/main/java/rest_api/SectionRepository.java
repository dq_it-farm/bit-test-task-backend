package rest_api;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface SectionRepository extends CrudRepository<Section, Long> {

    List<Section> findBySectionName(String sectionName);

    List<Section> findByJobId(Long jobId);

}
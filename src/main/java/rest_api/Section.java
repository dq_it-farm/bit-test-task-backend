package rest_api;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.ArrayList;

/**
 * Dataclass for JPA
 */
@Entity
public class Section {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Long jobId;
    private String sectionName;
    private ArrayList<Geodata> geodata = new ArrayList<>();

    protected Section() {
    }

    public Section(String sectionName, ArrayList<Geodata> geodata, Long jobId) {
        this.sectionName = sectionName;
        this.geodata = geodata;
        this.jobId = jobId;
    }

    public Long getJobId() {
        return jobId;
    }

    public void setSectionName(String sectionName) {
        this.sectionName = sectionName;
    }

    public void setGeodata(ArrayList<Geodata> geodata) {
        this.geodata = geodata;
    }

    public Long getId() {
        return id;
    }

    public String getSectionName() {
        return sectionName;
    }

    public ArrayList<Geodata> getGeodata() {
        return geodata;
    }

    public void setJobId(Long jobId) {
        this.jobId = jobId;
    }

    public void addGeodata(Geodata data) {
        geodata.add(data);

    }
    public void removeGeodata(Geodata data) {
        geodata.remove(data);

    }

}

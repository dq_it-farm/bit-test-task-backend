package rest_api;
import java.io.Serializable;

/**
 * Simple dataclass
 */
public class Geodata implements Serializable {

    private String className;
    private String classCode;

    protected Geodata() {
    }

    public Geodata(String className, String classCode) {
        this.className = className;
        this.classCode = classCode;
    }

    public String getClassName() {
        return className;
    }

    public String getClassCode() {
        return this.classCode;
    }

    @Override
    public String toString() {
        return className + " " + classCode;
    }

}